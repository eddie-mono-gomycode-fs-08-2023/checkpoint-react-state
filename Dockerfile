# Use an official Node.js LTS (Long Term Support) image as the base image
FROM node:18

# Set the working directory inside the container
WORKDIR /var/www/html

# Copy the the application code to the working directory
COPY . /var/www/html

# Install project dependencies
RUN npm install

# Expose the port that the app will run on
EXPOSE 3000

# Build the Vue.js project for production
RUN npm run build

# Set the default command to run the application
CMD ["npm", "run", "start"]