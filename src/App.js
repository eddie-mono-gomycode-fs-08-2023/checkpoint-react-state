import { useState } from 'react';

function App() {
  const [person, setPerson] = useState({
    fullName: 'May Weather',
    bio: "I am a boxer",
    imgSrc: 'https://upload.wikimedia.org/wikipedia/commons/7/7e/Floyd_Mayweather%2C_Jr._at_DeWalt_event_%285888721735%29_%28rotated_4%29.jpg',
    profession: 'Boxing',
  });
  const [shows, setShows] = useState(false);

  const toggleShow = () => {
    setShows(!shows);
  };

  return (
    <div>
      <button onClick={toggleShow}>Click to view profile</button>
      {shows && (
        <div  style={{display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
          <img src={person.imgSrc} alt="Avatar" style={{height: '300px', width: '20%'}} />
          <h2>{person.fullName}</h2>
          <p>{person.profession}</p>
          <p>{person.bio}</p>
        </div>
      )}
    </div>
  );
}

export default App;