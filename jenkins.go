pipeline {
	agent any

	environment {

			GIT_BRANCH = "master"
			GIT_URL = "https://gitlab.com/eddie-mono-gomycode-fs-08-2023/checkpoint-react-state.git"
			GIT_CREDENTIAL_ID = "glpat-ACsJVE1d3LMmzTMVHkej"

			APP_CONTAINER_NAME = "checkpoint-react-state"
			APP_IMAGE_NAME = "eddiemono/checkpoint-react-state"
			APP_PORT = "4000"
			SERVER_PORT = "3000"
			NETWORK = "checkpoint-react-state-net"
			RESTART_WAY = "always"
	}

	stages {
			stage("Pull project from git") {
					steps {
							git branch: "${GIT_BRANCH}", credentialsId: "${GIT_CREDENTIAL_ID}", url: "${GIT_URL}"
					}
			}

			stage("Create and start network") {
					steps {
							script {

									def containerExists = sh(script: "docker ps -a --format '{{.Names}}' | grep -w ${NETWORK}", returnStatus: true)
									
									if (containerExists == 0) {
											echo "create and start project network container: ${NETWORK}"
											bat "docker network create ${NETWORK}"
									}
									else{
											echo " ${containerExists} ${NETWORK} container already exist and start fine."
									}
							}
					}
			}


			stage("Build project image") {
					steps {
							script {
									bat "docker --version"
									bat "docker image build -t ${APP_IMAGE_NAME}:v$BUILD_ID ."
									bat "docker image tag ${APP_IMAGE_NAME}:v$BUILD_ID ${APP_IMAGE_NAME}:latest"
							}
					}
			}

			stage("Push project image on hub") {
					steps {
							script {
									withCredentials([usernamePassword(credentialsId: "dockerhub", passwordVariable: "pass", usernameVariable: "user")]) {
											bat "docker login -u ${user} -p ${pass}"
											bat "docker image push ${APP_IMAGE_NAME}:v$BUILD_ID"
											bat "docker image push ${APP_IMAGE_NAME}:latest"
											bat "docker image rmi ${APP_IMAGE_NAME}:v$BUILD_ID ${APP_IMAGE_NAME}:latest"
									}
							}
					}
			}

			stage("Remove project container if exist") {
					steps {
							script {

									def containerExists = sh(script: "docker ps -a --format '{{.Names}}' | grep -w ${APP_CONTAINER_NAME}", returnStatus: true)
									
									if (containerExists == 0) {
											echo " ${containerExists} ${APP_CONTAINER_NAME} container exist and will be remove and recreate."
											bat "docker stop ${APP_CONTAINER_NAME}"
											bat "docker rm ${APP_CONTAINER_NAME}"
									}
									else {
											echo "Docker container ${APP_CONTAINER_NAME} does not exist and will be create."
									}
							}
					}
			}

			stage("Create project container") {
					steps {
							script {

									echo "Create ${APP_CONTAINER_NAME} container."
									bat "docker run -itd --name ${APP_CONTAINER_NAME} --network ${NETWORK} -p ${APP_PORT}:${SERVER_PORT} ${APP_IMAGE_NAME}:latest"
							}
					}
			}

			stage("Delete all unuse containers and images") {
					steps {
							script {

									echo "Delete all unuse containers and images"
									bat "docker container prune -f"
									bat "docker image prune -a -f"
									bat "docker volume prune -a -f"
									bat "docker network prune -f"
									bat "docker system prune -f"
							}
					}
			}
	}
}